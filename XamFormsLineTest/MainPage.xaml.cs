﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Shapes;

namespace XamFormsLineTest
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            MyX.SizeChanged += drawX;
        }

        private void drawX(object sender, EventArgs e)
        {
            var container = (AbsoluteLayout)sender;
            double width = container.Width;
            double height = container.Height;
            double offset = container.Width * 0.3;

            // X: Line property values
            PenLineCap lineCap = PenLineCap.Round;
            double strokeThickness = 15;
            SolidColorBrush brush = new SolidColorBrush(Color.White);

            // X: Top left to bottom right
            Line line1 = new Line
            {
                X1 = container.X + offset,
                Y1 = container.X + offset,
                X2 = container.X + width - offset,
                Y2 = container.X + height - offset,
                Stroke = brush,
                StrokeLineCap = lineCap,
                StrokeThickness = strokeThickness,
            };

            // X: Bottom left to top right
            Line line2 = new Line
            {
                X1 = container.X + offset,
                Y1 = container.X + height - offset,
                X2 = container.X + width - offset,
                Y2 = container.X + offset,
                Stroke = brush,
                StrokeLineCap = lineCap,
                StrokeThickness = strokeThickness
            };

            container.Children.Add(line1);
            container.Children.Add(line2);

        }
    }
}
